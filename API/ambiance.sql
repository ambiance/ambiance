-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : mer. 19 mai 2021 à 19:43
-- Version du serveur :  5.7.31
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `ambiance`
--

-- --------------------------------------------------------

--
-- Structure de la table `chemin`
--

DROP TABLE IF EXISTS `chemin`;
CREATE TABLE IF NOT EXISTS `chemin` (
  `chemin` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `chemin`
--

INSERT INTO `chemin` (`chemin`) VALUES
('C:/PERSO/PROJET_AMBIANCE/playlist/top-6-gun-shot-sound-effects.mp3'),
('C:/PERSO/PROJET_AMBIANCE/playlist/moby-go-1991.mp3'),
('C:/PERSO/PROJET_AMBIANCE/playlist/Black Eyed Peas - I Gotta Feeling .wav'),
('C:/PERSO/PROJET_AMBIANCE/playlist/Micheal Jackson - Billie Jean.mp3'),
('C:/PERSO/PROJET_AMBIANCE/playlist/Micheal Jackson beat-it.mp3');

-- --------------------------------------------------------

--
-- Structure de la table `genre_musique`
--

DROP TABLE IF EXISTS `genre_musique`;
CREATE TABLE IF NOT EXISTS `genre_musique` (
  `ID_GENRE` int(11) NOT NULL,
  `LIBELLE_GENRE` char(15) DEFAULT NULL,
  PRIMARY KEY (`ID_GENRE`),
  UNIQUE KEY `GENRE_MUSIQUE_PK` (`ID_GENRE`),
  UNIQUE KEY `GENRE_MUSIQUE_IDX` (`ID_GENRE`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `moment`
--

DROP TABLE IF EXISTS `moment`;
CREATE TABLE IF NOT EXISTS `moment` (
  `ID_MUSIQUE` int(11) NOT NULL,
  `ID_TEMP` int(11) NOT NULL,
  `DATE` date DEFAULT NULL,
  PRIMARY KEY (`ID_MUSIQUE`,`ID_TEMP`),
  KEY `MOMENT_IDX` (`ID_MUSIQUE`,`ID_TEMP`),
  KEY `FK_MOMENT_MOMENT2_TEMPERAT` (`ID_TEMP`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `musique`
--

DROP TABLE IF EXISTS `musique`;
CREATE TABLE IF NOT EXISTS `musique` (
  `ID_MUSIQUE` int(11) NOT NULL,
  `ID_GENRE` int(11) DEFAULT NULL,
  `ID_TYPE` int(11) DEFAULT NULL,
  `TITRE` varchar(50) DEFAULT NULL,
  `NOM_ARTISTE` varchar(30) DEFAULT NULL,
  `DUREE` varchar(255) DEFAULT NULL,
  `CHEMIN` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID_MUSIQUE`),
  UNIQUE KEY `MUSIQUE_PK` (`ID_MUSIQUE`),
  UNIQUE KEY `MUSIQUE_IDX` (`ID_MUSIQUE`),
  KEY `RELATION_5_FK` (`ID_GENRE`),
  KEY `RELATION_5_IDX` (`ID_GENRE`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `musique`
--

INSERT INTO `musique` (`ID_MUSIQUE`, `ID_GENRE`, `ID_TYPE`, `TITRE`, `NOM_ARTISTE`, `DUREE`, `CHEMIN`) VALUES
(2, 3, 2, 'Beat it', 'Micheal Jackson', '04:18', 'C:/PERSO/PROJET_AMBIANCE/playlist/Micheal Jackson beat-it.mp3'),
(3, 3, 2, 'Billie Jean', 'Micheal Jackson', '04:55', 'C:/PERSO/PROJET_AMBIANCE/playlist/Micheal Jackson - Billie Jean.mp3'),
(4, 2, 4, 'I Gotta Feeling', 'Black Eyed Peas', '04:49', 'C:/PERSO/PROJET_AMBIANCE/playlist/Black Eyed Peas - I Gotta Feeling .wav'),
(5, 1, 2, 'party-rock-anthem', 'lmfao', '04:24', 'C:/PERSO/PROJET_AMBIANCE/playlist/lmfao-ft-lauren-bennett-goonrock-party-rock-anthem-official-audio.mp3'),
(6, 2, 2, 'Meet & Fun!', 'Ofshane', '02:08', 'C:/PERSO/PROJET_AMBIANCE/playlist/Meet & Fun! - Ofshane.mp3');

-- --------------------------------------------------------

--
-- Structure de la table `temperature`
--

DROP TABLE IF EXISTS `temperature`;
CREATE TABLE IF NOT EXISTS `temperature` (
  `ID_TEMP` int(11) NOT NULL,
  `TYPE_CLIMAT` char(1) DEFAULT NULL,
  `VALEUR_TEMP` float(30,0) NOT NULL,
  PRIMARY KEY (`ID_TEMP`),
  UNIQUE KEY `TEMPERATURE_PK` (`ID_TEMP`),
  UNIQUE KEY `TEMPERATURE_IDX` (`ID_TEMP`),
  KEY `RELATION_6_FK` (`TYPE_CLIMAT`),
  KEY `RELATION_6_IDX` (`TYPE_CLIMAT`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `type_climat`
--

DROP TABLE IF EXISTS `type_climat`;
CREATE TABLE IF NOT EXISTS `type_climat` (
  `TYPE_CLIMAT` char(1) NOT NULL,
  `LIBELLE_TYPE_CLIMAT` char(30) DEFAULT NULL,
  PRIMARY KEY (`TYPE_CLIMAT`),
  UNIQUE KEY `TYPE_CLIMAT_IDX` (`TYPE_CLIMAT`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
