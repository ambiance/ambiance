import os
from tkinter import *
from tkinter import ttk
from tkinter import filedialog
from pytube import YouTube #pip install pytube3
from moviepy.editor import * 
import youtube_dl





Nom_dossier = ""

#file location
#def openLocation():
    #global Nom_dossier
    #Nom_dossier = filedialog.askdirectory()
    #if(len(Nom_dossier) > 1):
    #    locationError.config(text=Nom_dossier,fg="green")

   # else:
     #   locationError.config(text="Veuillez choisir un dossier valid",fg="red")
        





#télécharge la vidéo
def DownloadVideo():
    choice = ytdchoices.get()
    url = ytdEntry.get()

    if(len(url)>1):
        ytdErreur.config(text="")
        yt = YouTube(url)

        if(choice == choices[0]):
            entree_url = url
            #obtenir le titre de la vidéo 
            video_info = youtube_dl.YoutubeDL().extract_info(url=entree_url, download=False)
            titre_video = video_info['title']

            #les réglages pour télécharger l'audio seulement en qualité 192 kHz  ( haut qualité ) 
            reglages = {
                'format': 'bestaudio/best',
                'outtmpl': f"C:/PERSO/PROJET_AMBIANCE/playlist/{titre_video}.mp3", #destination du téléchargement
                'postprocessors': [{
                    'key': 'FFmpegExtractAudio',
                    'preferredcodec': 'mp3',
                    'preferredquality': '192',
                }],
            }
            
            with youtube_dl.YoutubeDL(reglages) as ydl:
              ydl.download([entree_url]) 
            ytdtelecharge.config(text="Téléchargement Terminé !")

        if(choice == choices[1]):
            ytdErreur.config(text="cette fonctionnalité n'est pas encore disponible")
            



    


root = Tk()
root.title("Le téléchargeur Ambiance")
root.geometry("350x200") #set window
root.columnconfigure(0,weight=1)#set all content in center.
root.iconbitmap(r'favicon.ico') #attribuer l'icone

#Ytd Link Label
ytdLabel = Label(root,text="Veuillez entrer l'url de la vidéo",font=("jost",15))
ytdLabel.grid()

#Entry Box
ytdEntryVar = StringVar()
ytdEntry = Entry(root,width=50,textvariable=ytdEntryVar)
ytdEntry.grid()


ytdErreur = Label(root,text="",fg="red",font=("jost",10))
ytdErreur.grid()

ytdtelecharge = Label(root,text="",fg="green",font=("jost",10))
ytdtelecharge.grid()

#Asking save file label
#saveLabel = Label(root,text="Sauver le fichier vidéo",font=("jost",15,"bold"))
#saveLabel.grid()




ytdQuality = Label(root,text="Séléctionnez le format souhaité",font=("jost",15))
ytdQuality.grid()


choices = ["MP3","WAV"]
ytdchoices = ttk.Combobox(root,values=choices)
ytdchoices.grid()


downloadbtn = Button(root,text="Télécharger",width=15,bg="red",fg="white",command=DownloadVideo)
downloadbtn.grid()


root.mainloop()