#include "DHT.h" 
#define DHTPIN 7 // broche ou l'on a branche le capteur
#define DHTTYPE DHT11 // DHT 11
DHT dht(DHTPIN, DHTTYPE);//déclaration du capteur 
#include <WiFiNINA.h>


char ssid[] = "AMBIANCE";
char pass[] = "testtest";

int status = WL_IDLE_STATUS;

char server[] = "www.google.com";

String postData;
String postVariable = "temp=";

WiFiClient client;

void setup(){
 Serial.begin(9600);
 while (status != WL_CONNECTED) {
 Serial.print("Attempting to connect to Network named: ");
 Serial.println("DHTambiance test!");
 Serial.println(ssid);
 status = WiFi.begin(ssid, pass);
 delay(10000);
 }

 dht.begin();
 
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());
  IPAddress ip = WiFi.localIP();
  IPAddress gateway = WiFi.gatewayIP();
  Serial.print("IP Address: ");
  Serial.println(ip);
}


void loop(){
  int reading = analogRead(DHTPIN);
 delay(2000);
 // La lecture du capteur prend 250ms
 // Les valeurs lues peuvet etre vieilles de jusqu'a 2 secondes (le capteur est lent)
 float t = dht.readTemperature();//on lit la temperature en celsius (par defaut)
 //On verifie si la lecture a echoue, si oui on quitte la boucle pour recommencer.
 if (isnan(t)){
   Serial.println("Failed to read from DHT sensor!");
   return;
   postData = reading ;

 if (client.connect(server, 80)) {
    client.println("POST /test/post.php HTTP/1.1");
    client.println("Host: www.elithecomputerguy.com");
    client.println("Content-Type: application/x-www-form-urlencoded");
    client.print("Content-Length: ");
    client.println(postData.length());
    client.println();
    client.print(postData);
  }
if (client.connected()) {
    client.stop();
  }
  Serial.println(postData);

  delay(3000);

   
 }
 //Affichages :
 Serial.print("Temperature: ");
 Serial.print(t);
 Serial.print(" *C ");
}
