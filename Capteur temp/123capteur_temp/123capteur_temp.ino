#include "DHT.h" 
#define DHTPIN 7 // broche ou l'on a branche le capteur
#define DHTTYPE DHT11 // DHT 11
DHT dht(DHTPIN, DHTTYPE);//déclaration du capteur 

void setup(){
 Serial.begin(9600);
 Serial.println("DHTambiance test!");
 dht.begin();
}
void loop(){
 delay(2000);
 // La lecture du capteur prend 250ms
 // Les valeurs lues peuvet etre vieilles de jusqu'a 2 secondes (le capteur est lent)
 float t = dht.readTemperature();//on lit la temperature en celsius (par defaut)
 //On verifie si la lecture a echoue, si oui on quitte la boucle pour recommencer.
 if (isnan(t)){
   Serial.println("Failed to read from DHT sensor!");
   return;
 }
 //Affichages :
 Serial.print("Temperature: ");
 Serial.print(t);
 Serial.print(" *C ");
}
