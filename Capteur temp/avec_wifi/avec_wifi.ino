#include "DHT.h"
#include <WiFiNINA.h> 
#define DHTPIN 4 // broche ou l'on a branche le capteur
#define DHTTYPE DHT11 // DHT 11

DHT dht(DHTPIN, DHTTYPE);//déclaration du capteur


char ssid[] = "test" ; //ssid du wifi
char pass[] = "";

int status = WL_IDLE_STATUS;

char server[] = "http://127.0.0.1:8000/temp/";

String postData;
String postVariable = "temp=";

WiFiClient client;
 
void setup()
{
 Serial.begin(9600);

 while (status != WL_CONNECTED) {
  Serial.print("Tentative de connexion  au serveur:");
  Serial.print(ssid);
  status = WiFi.begin(ssid,pass);
  delay(10000);
  
  }

 Serial.println("SSID: ");
 Serial.println(WiFi.SSID());
 IPAddress ip = WiFi.localIP();
 IPAddress gateway = WiFi.gatewayIP();
 Serial.print("IP Address:");
 Serial.println(ip);
 
 Serial.println("DHTambiance test!");
 dht.begin();

}






void loop()
{
 delay(2000);
 
 // La lecture du capteur prend 250ms
 // Les valeurs lues peuvet etre vieilles de jusqu'a 2 secondes (le capteur est lent)
 float t = dht.readTemperature();//on lit la temperature en celsius (par defaut)
 
 //On verifie si la lecture a echoue, si oui on quitte la boucle pour recommencer.
 if (isnan(t))
 {
   Serial.println("Failed to read from DHT sensor!");
   return;
 }
 
  postData = t; // on stock la vaiable de température dans notre variable potData
 //Affichages :

 Serial.print("Temperature: ");
 Serial.print(t);
 Serial.print(" *C ");

 if (client.connect(server,80)){
    //client.println("POST http://127.0.0.1:8000/temp/");
    client.println("Host: http://127.0.0.1:8000/temp/");
    client.println(postData.length());
    client.println();
    client.print(postData);
 }

  Serial.println(postData);

}
