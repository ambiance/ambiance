import builtins
import os 
import threading
from tkinter import *
from tkinter import filedialog
from tkinter import ttk
from ttkthemes import themed_tk as tk
from mutagen.mp3 import MP3
import tkinter.messagebox
from pygame import mixer
import pygame
import time
#import pandas as pd
import datetime
import mysql.connector
import sys
import random

mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  password="",
  database="ambiance"

)
cursor = mydb.cursor()






pause = False

root = tk.ThemedTk() #creation d'une fenetre à l'interieur de variable tk"
root.get_themes()
root.set_theme("radiance")

status = ttk.Label(root,text="Bienvenu dans le lecteur Ambiance !",relief = SUNKEN, anchor=W , font='Time 10 italic')
status.pack(side=BOTTOM, fill_= X)#afficher le texte en bas rempli l'axe des x

#création d'un menu
menu = Menu(root)
root.config(menu=menu)

#créer les sous menu

playlist = []

finjouer = True


#playlist = contiens la path + le nom du fichier
#playlistbox = contiens le nom du fichier
# Fullpath + nomfichiervest requis pour jouer la musique a l'interieur de play_music


def explorer_fichier():
    global filename_path #je peux utiliser cette variable partout
    cursor.execute("SELECT chemin FROM musique WHERE ID_TYPE = '2'")
    result = cursor.fetchall()
    print (result)
    for row in result:
        filename_path = row[0]
        ajout_playlist(filename_path)
        print(row[0])
        print("\n") 
    

def ajout_playlist(filename):
    index = 0
    playlist.insert(index, filename)
    filename = os.path.basename(filename) #pour afficher seulement le nom du fichier sans path
    playlistbox.insert(index,filename)

    print(playlist)
    playlistbox.pack()
    index += 1 #incrémentation de l'index




sousMenu = Menu(menu, tearoff=0)
menu.add_cascade(label="Fichier",menu=sousMenu)
sousMenu.add_command(label="Ouvrir",command = explorer_fichier)
sousMenu.add_command(label="Quitter", command = root.destroy)#fonction pour fermer la page



def a_propos():
    tkinter.messagebox.showinfo('Projet Ambiance','Le projet Ambiance a pour but d améliorer la vie lycéenne . Il s agit là d un lecteur de musique codé en Python par Mr.Lounis Hamroun')
sousMenu = Menu(menu, tearoff=0)
menu.add_cascade(label="Aide",menu=sousMenu)
sousMenu.add_command(label="À propos",command = a_propos)



mixer.init() #initialisation le mixer audio
pygame.init() #initialisation le mixer audio


#root.geometry('400x400')#taille fenetre
root.title("AMBIANCE")#titre fenetre
root.iconbitmap(r'favicon.ico') #attribuer l'icone

leftframe = Frame(root)
leftframe.pack(side=LEFT,padx=30,pady=30)

playlistbox = Listbox(leftframe)
playlistbox.pack()


btnaj = ttk.Button(leftframe, text="+ Ajouter",command= explorer_fichier)
btnaj.pack(side=LEFT)

def eff_musique():
    #musique_select = playlistbox.curselection()
    musique_select = filename_path
    musique_select = int(musique_select[0])
    playlistbox.delete(musique_select)
    playlist.pop(musique_select)


btnef = ttk.Button(leftframe, text="- Effacer", command=eff_musique)
btnef.pack(side=LEFT)

rightframe = Frame(root)
rightframe.pack(pady=30)


#filelabel = Label(root,text = 'Et la musique fut !')
#filelabel.pack(pady=10)

topframe = Frame(rightframe)#le cadre est dans le cadre de droite
topframe.pack()

lengthlabel = ttk.Label(topframe,text = 'Durée  :  --:--' , font='Arial 10 bold')
lengthlabel.pack(pady=5)

currenttimelabel = ttk.Label(topframe,text = 'Temps écoulé :  --:--' , relief = GROOVE)
currenttimelabel.pack(pady=10)



def show_details(play_music):
    #filelabel['text'] = "Musique Jouée "+ '  -  ' + os.path.basename(filename)

    file_data = os.path.splitext(play_music)

    if file_data[1] == ".mp3":
        audio = MP3(play_music)
        longueurtotal = audio.info.length

    else:
        a = mixer.Sound(play_music)
        longueurtotal = a.get_length()

    # div = lognueurtotal/60 , met le resultat dans mins et le reste dans secondes
    mins,secs = divmod (longueurtotal,60)
    mins = round(mins)#arrondir la valeur
    secs = round(secs)#arrondir la valeur
    timeformat = '{:02d}:{:02d}'.format(mins,secs)
    lengthlabel['text'] = "Durée "+ '  -  ' + timeformat

    t1 = threading.Thread(target=start_count,args=(longueurtotal,))
    t1.start()#

    

def start_count(t):
    global paused
    moment=0
    while ((moment<= t) and (finjouer == False)):# and mixer.music.get_busy():
        if paused:
            continue #pour stopper le compteur lorsque la musique s'arrete
        else:
            mins,secs = divmod (moment,60)
            mins = round(mins)#arrondir la valeur
            secs = round(secs)#arrondir la valeur
            timeformat = '{:02d}:{:02d}'.format(mins,secs)#format affichage
            currenttimelabel['text'] = "Temps écoulé  "+ '  -  ' + timeformat
            time.sleep(1)#1 seconde
            moment += 1
    if (finjouer):
        currenttimelabel['text'] = "Temps écoulé  " + '  -  ' + "--:--"
        lengthlabel['text'] = "Durée " + '  -  ' + "--:--"


    
    

def play_music():#definition de la fonction

    global paused
    global index
    global finjouer

    if paused:
        mixer.music.unpause()
        status['text'] = "Musique Reprise"
        paused = FALSE
    else:
        try:
                #while TRUE:
                    remplir_playList_ajouer()
                    finjouer = False
                    time.sleep(1)
                    dt = datetime.datetime.today()#genere la date du jour
                    nombre_musiques = len(playlist2)#nombre de musique dans la liste
                    musique_tire = dt.second%nombre_musiques#divise le nombre de l'heure par le nombre de musique
                    #play_it = playlist[musique_tire]
                    play_it = playlist2.pop()
                    #play_it = 'C:/tmp/playlist/a-music-sounds-better-with-you.mp3'
                    pygame.mixer.music.load(play_it)#mixer charge le fichier audio
                    status['text'] = "Musique lancée " + '  -  ' + os.path.basename(play_it)
                    show_details(play_it)

                    #play_it = playlist2.pop()
                    #pygame.mixer.music.queue(play_it)  # Q
                    pygame.mixer.music.set_endevent(pygame.USEREVENT)  # Setup the end track event
                    pygame.mixer.music.play()  # mixer lance le fichier

                    # Lancer un thread pour la suite
                    d = threading.Thread(name='daemon', target=play_next_paylist, args =(lambda : playlist2, ))
                    #d.setDaemon(True)
                    d.start()

        except:
         tkinter.messagebox.showerror('La musique n a pas pu être trouvée','Veuillez verifier l emplacement du fichier.' + play_it)
         print("Error", sys.exc_info()[1])


def play_next_paylist(stop):

    while (finjouer == False):
        time.sleep(1)
        for event in pygame.event.get():
            if event.type == pygame.USEREVENT:  # A track has ended
                if ((finjouer == False) and (len(playlist2) > 0)):  # If there are more tracks in the queue...
                    play_it = playlist2.pop()
                    pygame.mixer.music.load(play_it)
                    pygame.mixer.music.play()  # mixer lance le fichier
                    status['text'] = "Musique lancée " + '  -  ' + os.path.basename(play_it)
                    show_details(play_it)

                elif (finjouer == False):
                    remplir_playList_ajouer()
                    play_it = playlist2.pop()
                    pygame.mixer.music.load(play_it)
                    pygame.mixer.music.play()  # mixer lance le fichier
                    status['text'] = "Musique lancée " + '  -  ' + os.path.basename(play_it)
                    show_details(play_it)


def nextsong(event):
    if ((finjouer == False) and (len(playlist2) > 0)):
        mixer.music.stop()#mixer stop le fichier

    #global index
    #index += 1
    #pygame.mixer.music.load(play_it)
    #pygame.mixer.music.play()



def stop_music():#definition de la fonction
    global finjouer
    global playlist2

    mixer.music.stop()  # mixer stop le fichier
    status['text'] = "Musique stoppée"
    playlist2 = []
    finjouer = True




paused = FALSE
def pause_music():
    global paused
    paused = TRUE
    mixer.music.pause()
    status['text'] = "Musique en pause"

def rewind_music():
    global finjouer
    if ((finjouer == False) and (len(playlist2) > 0)):
        mixer.music.stop()#mixer stop le fichier
        status['text'] = "Musique suivante"
    #play_music()


def set_Vol(val):#string il faut convertir en integer
    volume = float(val)/100 #conversion en string to integer typecasting /100 car mixer prend valeur entre 0 et 1 
    mixer.music.set_volume(volume)

muted = FALSE #création d'une variable muted
def mute_music():
    global muted #cette fonction peut être utiliser partout dans le programme
    if muted: #démuter la musique
        mixer.music.set_volume(0.8) #démuter la musique , valeur= 80% ( par défaut) , volume à 80 sur le curseur
        unmuteBtn.configure(image=unmutePhoto)
        curseur_volume.set(80)
        muted = FALSE
    
    else: 
        mixer.music.set_volume(0) #muter la musique , valeur= 0% , volume nul
        unmuteBtn.configure(image=mutePhoto)
        curseur_volume.set(0)
        muted = TRUE
    
    

middleframe = Frame (rightframe)
middleframe.pack(pady=30,padx=30)#mettre le  cadre du millieu sur tous boutons

playPhoto = PhotoImage(file='IMAGES/play-button.png')
playBtn = ttk.Button(middleframe,image=playPhoto , command = play_music)#le bouton play
playBtn.pack(side=LEFT,padx=10)#emboiter


stopPhoto = PhotoImage(file='IMAGES/stop-button.png')
stopBtn = ttk.Button(middleframe,image=stopPhoto ,command = stop_music)#le bouton stop + appel fonction pour arreter la musique
stopBtn.pack(side=LEFT,padx=10)#emboiter

pausePhoto = PhotoImage(file='IMAGES/pause-button.png')
pauseBtn =ttk.Button(middleframe,image=pausePhoto , command = pause_music)#le bouton stop + appel fonction pour arreter la musique
pauseBtn.pack(side=LEFT,padx=10)#emboiter

bottomframe = Frame (rightframe)
bottomframe.pack()#créer un cadre en dessous du cadre du milleu

rewindPhoto = PhotoImage(file='IMAGES/forward-button .png')
rewindBtn = ttk.Button(bottomframe,image=rewindPhoto , command = rewind_music)
rewindBtn.grid(row=0,column=0)#boutton apparait au rang 0 colonne 0

mutePhoto = PhotoImage(file='IMAGES/mute-button.png')
unmutePhoto = PhotoImage(file='IMAGES/unmute-button.png')
unmuteBtn = ttk.Button(bottomframe,image=unmutePhoto , command = mute_music)
unmuteBtn.grid(row=0,column=1)#boutton apparait au rang 0 colonne 0

curseur_volume = ttk.Scale(bottomframe,from_=0,to=100, orient=HORIZONTAL , command = set_Vol)#appel de la fonction pour stocker la valeur sur lequel est le curseur
curseur_volume.set(10)
mixer.music.set_volume(0.1)
curseur_volume.grid(row=0,column=2,pady=14,padx=30)


def  remplir_playList_ajouer():
    global playlist2
    playlist2 = list()
    for song in playlist:
        playlist2.append(song)
    random.shuffle(playlist2)
    print(playlist2)

def fermeture():
    stop_music() #fonction stopper la musique avant de detruire le programmme
    root.destroy()#fonction de destruction du programme
    sys.exit()

root.protocol("WM_DELETE_WINDOW",fermeture)

root.mainloop()
#ajout
#connection à la database








