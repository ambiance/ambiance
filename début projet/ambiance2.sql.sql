/*==============================================================*/
/* Nom de SGBD :  MySQL 5.0                                     */
/* Date de cr�ation :  29/03/2021 08:32:09                      */
/*==============================================================*/


drop table if exists GENRE_MUSIQUE;

drop table if exists MOMENT;

drop table if exists MUSIQUE;

drop table if exists TEMPERATURE;

drop table if exists TYPE_CLIMAT;

/*==============================================================*/
/* Table : GENRE_MUSIQUE                                        */
/*==============================================================*/
create table GENRE_MUSIQUE
(
   ID_GENRE             int not null,
   LIBELLE_GENRE        char(15),
   primary key (ID_GENRE)
);

/*==============================================================*/
/* Table : MOMENT                                               */
/*==============================================================*/
create table MOMENT
(
   ID_MUSIQUE           int not null,
   ID_TEMP              int not null,
   DATE                 date,
   primary key (ID_MUSIQUE, ID_TEMP)
);

/*==============================================================*/
/* Table : MUSIQUE                                              */
/*==============================================================*/
create table MUSIQUE
(
   ID_MUSIQUE           int not null,
   ID_GENRE             int,
   ID_TYPE              int,
   TITRE                varchar(50),
   NOM_ARTISTE          varchar(30),
   DUREE                decimal(6),
   primary key (ID_MUSIQUE)
);

/*==============================================================*/
/* Table : TEMPERATURE                                          */
/*==============================================================*/
create table TEMPERATURE
(
   ID_TEMP              int not null,
   TYPE_CLIMAT          char(1),
   VALEUR_TEMP          decimal(30) not null,
   primary key (ID_TEMP)
);

/*==============================================================*/
/* Table : TYPE_CLIMAT                                          */
/*==============================================================*/
create table TYPE_CLIMAT
(
   TYPE_CLIMAT          char(1) not null,
   LIBELLE_TYPE_CLIMAT  char(30),
   primary key (TYPE_CLIMAT)
);

alter table MOMENT add constraint FK_MOMENT foreign key (ID_MUSIQUE)
      references MUSIQUE (ID_MUSIQUE) on delete restrict on update restrict;

alter table MOMENT add constraint FK_MOMENT2 foreign key (ID_TEMP)
      references TEMPERATURE (ID_TEMP) on delete restrict on update restrict;

alter table MUSIQUE add constraint FK_RELATION_5 foreign key (ID_GENRE)
      references GENRE_MUSIQUE (ID_GENRE) on delete restrict on update restrict;

alter table TEMPERATURE add constraint FK_RELATION_6 foreign key (TYPE_CLIMAT)
      references TYPE_CLIMAT (TYPE_CLIMAT) on delete restrict on update restrict;

