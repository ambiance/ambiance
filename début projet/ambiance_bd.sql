-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  lun. 08 mars 2021 à 08:51
-- Version du serveur :  5.7.26
-- Version de PHP :  7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `ambiance_bd`
--

-- --------------------------------------------------------

--
-- Structure de la table `genre_musique`
--

DROP TABLE IF EXISTS `genre_musique`;
CREATE TABLE IF NOT EXISTS `genre_musique` (
  `ID_GENRE` int(11) NOT NULL,
  `LIBELLE_GENRE` char(15) DEFAULT NULL,
  PRIMARY KEY (`ID_GENRE`),
  UNIQUE KEY `GENRE_MUSIQUE_PK` (`ID_GENRE`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `moment`
--

DROP TABLE IF EXISTS `moment`;
CREATE TABLE IF NOT EXISTS `moment` (
  `ID_MUSIQUE` int(11) NOT NULL,
  `ID_TEMP` int(11) NOT NULL,
  `DATE` date DEFAULT NULL,
  PRIMARY KEY (`ID_MUSIQUE`,`ID_TEMP`),
  UNIQUE KEY `MOMENT_PK` (`ID_MUSIQUE`,`ID_TEMP`),
  KEY `MOMENT_FK` (`ID_MUSIQUE`),
  KEY `MOMENT2_FK` (`ID_TEMP`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `musique`
--

DROP TABLE IF EXISTS `musique`;
CREATE TABLE IF NOT EXISTS `musique` (
  `ID_MUSIQUE` int(11) NOT NULL,
  `ID_GENRE` int(11) DEFAULT NULL,
  `ID_TYPE` int(11) DEFAULT NULL,
  `TITRE` varchar(50) DEFAULT NULL,
  `NOM_ARTISTE` varchar(30) DEFAULT NULL,
  `DUREE` decimal(6,0) DEFAULT NULL,
  PRIMARY KEY (`ID_MUSIQUE`),
  UNIQUE KEY `MUSIQUE_PK` (`ID_MUSIQUE`),
  KEY `RELATION_5_FK` (`ID_GENRE`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `temperature`
--

DROP TABLE IF EXISTS `temperature`;
CREATE TABLE IF NOT EXISTS `temperature` (
  `ID_TEMP` int(11) NOT NULL,
  `TYPE_CLIMAT` char(1) DEFAULT NULL,
  `VALEUR_TEMP` decimal(30,0) NOT NULL,
  PRIMARY KEY (`ID_TEMP`),
  UNIQUE KEY `TEMPERATURE_PK` (`ID_TEMP`),
  KEY `RELATION_6_FK` (`TYPE_CLIMAT`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `type_climat`
--

DROP TABLE IF EXISTS `type_climat`;
CREATE TABLE IF NOT EXISTS `type_climat` (
  `TYPE_CLIMAT` char(1) NOT NULL,
  `LIBELLE_TYPE_CLIMAT` char(30) DEFAULT NULL,
  PRIMARY KEY (`TYPE_CLIMAT`),
  UNIQUE KEY `TYPE_CLIMAT_PK` (`TYPE_CLIMAT`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
